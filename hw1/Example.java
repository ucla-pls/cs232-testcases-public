// this example tests whether functions calls in
// if/else and while loop are analyzed correctly

class Example
{
  public static void main(String[] args)
  {
        System.out.println(new NumberGame().calc1(23));
        System.out.println(new NumberGame().calc2(46));
    }
}

class NumberGame
{
  int number1;
  int number2;
  int result;

  public int calc1(int rhs)
  {
    number1 = 90;
    if (rhs < number1) {
      result = this.sub(rhs, number1);
    } else {
      result = this.add(rhs, number1);
    }

    return result;
  }

  public int calc2(int rhs)
  {
    number2 = 30;
    while(number2 < 10)
    {
      number2 = this.sub(number2, rhs);
    }
    return number2;
  }

  public int add(int x, int y)
  {
    return x+y;
  }

  public int sub(int x, int y)
  {
    return x-y;
  }

}
