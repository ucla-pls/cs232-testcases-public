// // Here CFA would output more edges than 0CFA

class A {
    public static void main(String[] a){
        System.out.println(new B().F());
        System.out.println(new B().M());
    }
}

class B  {
    public int F(){
        return 0;
    }

    public int M() {
        return 0;
    }

}

class C extends B {

    public int F()
    {
        return 0;
    }

}


