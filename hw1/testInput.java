// The aspect that makes this test case interesting is that it uses an method that is defined in the base class..

class testInput {
	public static void main(String[] args){
    		Animal a;
		Dog d;
		int distA;
		int distD;
		int ret;

    		a = new Animal();
		ret = a.makeSound();
		distA = a.walk(10, 5);
		d = new Dog();
		ret = d.makeSound();
		distD = d.walk(10, 5);
	}

}

class Animal {
	public int makeSound(){
		System.out.println(false);
		return 0;
	}
	public int walk(int availableSpaces, int moves){
		int val;
		if(moves < availableSpaces){
			val = availableSpaces - moves;
		}
		else{
			val = 0;
		}
		return val;
	}


}

class Dog extends Animal {
	public int makeSound(){
		System.out.println(true);
		return 0;
	}
}
