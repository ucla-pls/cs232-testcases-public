/*
** A negative example where it is initiated before called;
** Test simple inheritance as well
*/


class CheckNullInit {
  public static void main(String[] a){
    System.out.println(new A().calculate(10));
  }
}

class A{
  B b;
  int tmp;

  public int calculate(int num) {
    b = new B().initialValue();
    System.out.println(b.returntwo());
    return new B().returnthree();
  }

  public B initialValue() {
    B c;
    c = new B();
    return c;
  }
}

class B extends A{
  public int returntwo() {
    return 2;
  }

  public int returnthree() {
    return 3;
  }
}